**HSBC FUll Stack API**
----

* To run apis Application
 
1. npm install
2. nodemon server.js or node server.js
 
** Root URL will be http://localhost:3000/admin-api

* I have exported postman collection in the backend part of it.
 
# Feature - Request rate limiting for the server

* For this you need to install Redis and "express-limiter" npm package to limit request rate. 
 
# Redis installation

* sudo apt-get install redis-server
* enable  service - sudo systemctl enable redis-server.service 
* restart service - sudo systemctl restart redis-server.service

* If you want to put limit please add this.rateLimiter.usingRemoteAddress() as second paramer in index.js of club folder.
 
* rate limit - 25 requests you can change it by changing limit for perticular user by ip by changing property of total in rate-limit-config.js




