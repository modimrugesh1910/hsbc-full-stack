'use strict';
var MongoClient = require('mongodb');
var _ = require('lodash');
const RateLimiter = require('../../libs/rate-limit-config');

module.exports = class ClubController {
    constructor(app) {
        this.rateLimiter = new RateLimiter(app);
        // apply it to add rate limit for apis
        // app.get('/apis/get_player_info/:name', this.rateLimiter.usingRemoteAddress(), this.get_player_info);
        app.get('/apis/get_data', this.get_data);
        app.get('/apis/get_player_info/:name', this.get_player_info);
        app.get('/apis/get_team_player_list/:name', this.get_team_player_list);
    }

    /**
     * @method get_player_info
     * @description  Method to get get_player_info
     * @param req
     * @param res
     */
    get_data(req, res) {
        MongoClient.connect("mongodb://localhost:27017/", function (err, db) {
            if (err) throw err;

            const dbo = db.db("hsbc");
            dbo.collection("personal_details").find({}).toArray(function (err, data) {
                if (err) throw err;
                res.send(data);
            });
        });
    }

    /**
     * @method get_player_info
     * @description  Method to get get_player_info
     * @param req
     * @param res
     */
    get_player_info(req, res) {
        MongoClient.connect("mongodb://localhost:27017/", function (err, db) {
            if (err) throw err;

            const dbo = db.db("hsbc");
            dbo.collection("personal_details").find({ "Name" : req.params.name}).toArray(function (err, data) {
                if (err) throw err;
                let response = {};
                response.Team = data[0]['Team'];
                response.Name = data[0]['Name'];
                response.Nationality = data[0]['Nationality'];
                response.Captain = data[0]['Is Captain(1=yes)'];
                response.Wktkeeper = data[0]['Is Wktkeeper(1=Yes)'];
                response.Batsman = data[0]['Is batsman'];
                response.Bowler = data[0]['Is bowler'];

                dbo.collection("batting_stat").find({ "Name" : req.params.name}).toArray(function (err, dta) {
                    if (err) throw err;
                    response.Matches = data[0]['Matches'];
                    response.Player_value = data[0]['Player Value USD'];
                    response.Innings_played = data[0]['Innings played'];
                    response.Not_out = data[0]['Not out'];
                    response.Runs_scored = data[0]['Runs scored'];
                    response.Highest_score = data[0]['Highest score'];
                    response.Batting_avg = data[0]['Batting avg'];
                    response.Balls_faced = data[0]['Balls faced'];
                    response.Strike_rate = data[0]['Strike rate'];
                    response.hundred_runs = data[0]['100 runs made'];
                    response.fifty_runs = data[0]['50 runs made'];
                    response.fours = data[0]['4s'];
                    response.sixs = data[0]['6s'];

                    dbo.collection("bowling_stat").find({ "Name" : req.params.name}).toArray(function (err, dta) {
                        if (err) throw err;
                        response.balls_bowled = data[0]['Number of balls bowled'];
                        response.Runs_given = data[0]['Runs given'];
                        response.Wkts_taken = data[0]['Wkts taken'];
                        response.Bowling_econ = data[0]['Bowling econ'];
                        response.Five_Wicket = data[0]['5 Wicket hauls'];
                        console.log("get_player_info api ===>" , response);
                        res.send(response);
                    });
                });
            });
        });
    }

    /**
     * @method get_team_player_list
     * @description  Method to get get_team_player_list
     * @param req
     * @param res
     */
    get_team_player_list(req, res) {
        MongoClient.connect("mongodb://localhost:27017/", function (err, db) {
            if (err) throw err;

            const dbo = db.db("hsbc");
            dbo.collection("personal_details").find({ "Team" : req.params.name}).toArray(function (err, data) {
                if (err) throw err;
                res.send(data);
            });
        });
    }
};