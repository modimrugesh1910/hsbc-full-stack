const client = require('./redis-database');
class Limiter{
    constructor(app){
        this.limiter = require('express-limiter')(app, client);
    }
    /**
     * For IP only
     */
    usingRemoteAddress() {
        console.log('limit user requests');
        return this.limiter({
            path: '/apis',
            method: 'get',
            lookup: ['connection.remoteAddress'],
            total: 25,
            expire: 1000 * 60 * 60,
            onRateLimited: function (request, response, next) {
                response.status(429).json('You are not welcome here, Rate limit exceeded');
            }
        });
    }
}

module.exports = Limiter;