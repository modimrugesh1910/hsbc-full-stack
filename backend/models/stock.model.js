module.exports = (mongoose) => {
    /**
     * Stock Schema
     */
    let StockSchema = new mongoose.Schema({
        date: {type: String},
        symbol: {type: String},
        open: {type: Number},
        close: {type: Number},
        low: {type: Number},
        high: {type: Number},
        volume: {type: Number},
    });
    return mongoose.model('Stock', StockSchema);
};