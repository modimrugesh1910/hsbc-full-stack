var _ = require('lodash');
module.exports = class AuthController {
    constructor(app) {
        app.get('/logout', this.doLogout);
        app.post('/checkLogin', this.checkLogin);
        app.post('/login', this.doLogin);
        app.get('/getuser/:id', this.getUser);
    }

    checkLogin(req, res) {
        if (req.session.isLoggedIn == 'Y') {
            res.send({status: 'success', data: req.session.userProfile});
        } else {
            res.status(401);
            res.send({});
        }
    }

    getUser(req, res) {
        let id = req.params.id;
        global.MongoORM.User.findOne({_id: id}, function (error, user) {
            if (!error)
                res.sendResponse(user);
            else
                res.sendError(error);
        });
    }

    doLogin(req, res) {
        let Email = req.body.Email,
            Password = req.body.Password;
        let promise = global.MongoORM.User.findOne({
            Email: Email,
            Password: Utils.md5(Password)
        }, {Password: false}).populate('Role');
        promise
            .then(function (userData) {
                if (userData) {
                    userData = JSON.parse(JSON.stringify(userData));
                    userData.Tokens = responseTokens;
                    req.session.isLoggedIn = 'Y';
                    req.session.userProfile = userData;
                    return res.send({status: "success", data: userData});
                } else {
                    return res.send({status: "failure", message: "Login failed"});
                }
            })
            .catch(function (error) {
                return res.send({status: "failure", message: "Login failed"});
            });
    }

    doLogout(req, res) {
        req.session.isLoggedIn = 'N';
        req.session.destroy();
        res.send({message: 'Logged out!'})
    }
};
