const express = require('express');
const bodyParser = require('body-parser');
const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');
const compression = require('compression');
const app = express();
const cors = require('cors');
const logger = require('morgan');
const path = require('path');
global.ROOT_PATH = __dirname;
const port = 3000;

app.use(cors());
app.use(bodyParser.urlencoded({extended: true, limit: '10mb'}));
app.use(bodyParser.json({limit: '10mb'}));
app.use(busboy());
app.use(busboyBodyParser());
app.use(compression());

app.use(express.static(path.join(__dirname, 'routes')));

let adminAPI=require('./apis');
new adminAPI(app);

// require('./app/routes')(app);
app.listen(port, () => {
    console.log('We are live on ' + port);
});