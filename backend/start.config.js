module.exports = {
  apps: [{
      "name": 'APIS',
      "script": 'server.js',
      "instances": 1,
      "exec_mode": "cluster",
      "max_memory_restart": "1000M",
      "node_args": "--max_old_space_size=1000"
    },
    {
      "name": "HOURLY",
      "script": "./app/workers/hourly.js",
      "instances": 1,
      "exec_mode": "fork",
      "max_memory_restart": "1000M",
      "node_args": "--max_old_space_size=1000"
    },
    {
      "name": "NOTIFICATIONS",
      "script": "./app/workers/notifications.js",
      "instances": 1,
      "exec_mode": "fork",
      "max_memory_restart": "1000M",
      "node_args": "--max_old_space_size=1000"
    },
    {
      "name": "AQI",
      "script": "./app/workers/aqi.js",
      "instances": 1,
      "exec_mode": "fork",
      "max_memory_restart": "1000M",
      "node_args": "--max_old_space_size=1000"
    },
    {
      "name": "DB_BACKUP",
      "script": "./app/workers/dbBackUp.js",
      "instances": 1,
      "exec_mode": "fork",
      "max_memory_restart": "1000M",
      "node_args": "--max_old_space_size=1000"
    }
  ]
};
