import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule, FormBuilder, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';

// import {FlexLayoutModule} from '@angular/flex-layout';

import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatToolbarModule,
    MatStepperModule, MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule, MatSnackBar, MatSnackBarModule, MatGridListModule
} from '@angular/material';

import 'hammerjs';

// in app components
import {FeatureTableComponent} from './feature-table/feature-table.component';

// in app routes
import {AppRoutes} from './app.routes';
import {AppService} from "./shared/app.service";

@NgModule({
    declarations : [
        AppComponent,
        FeatureTableComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,

        // Material
        MatButtonModule,
        MatCardModule,
        MatGridListModule,
        MatCheckboxModule,
        MatDialogModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatSelectModule,
        MatSidenavModule,
        MatSlideToggleModule,
        MatTabsModule,
        MatToolbarModule,
        MatStepperModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatSnackBarModule,
        // Flex-layout
        // FlexLayoutModule,

        // user routes
        AppRoutes,
        MatGridListModule
    ],
    providers : [
        FormBuilder,
        AppService,
        MatSnackBar
    ],
    exports : [
    ],
    entryComponents : [],
    bootstrap : [AppComponent]
})
export class AppModule {
}
