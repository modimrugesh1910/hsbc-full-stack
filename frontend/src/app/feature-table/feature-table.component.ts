import {Component, OnInit, AfterViewInit, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import {AppService} from "../shared/app.service";

@Component({
    selector: 'app-feature-table',
    templateUrl: './feature-table.component.html',
    styleUrls: ['feature-table.component.scss'],
    preserveWhitespaces: false
})

export class FeatureTableComponent implements OnInit, AfterViewInit, OnDestroy {
    /** Collection of subscribed variables */
    subscriptions: Subscription[] = [];
    /** bblDetails container */
    bblDetails: MatTableDataSource<any>;
    bblPlayerDetails: Array<any> = [];
    bblTeamDetails: Array<any> = [];
    /** display column header */
    displayedColumns = ['Team', 'Name', 'Nationality', 'Is Captain(1=yes)', 'Is Wktkeeper(1=Yes)', 'Player Value USD', 'Matches', 'Innings played', 'Not out', 'Runs scored', 'Highest score', 'Is batsman', 'Batting avg', 'Balls faced', 'Strike rate', '100 runs made', '50 runs made', '4s', '6s', 'Catches per match', 'Catches taken', 'Matches played', 'Is bowler?', 'Number of balls bowled', 'Runs given', 'Wkts taken', 'Bowling econ', '5 Wicket hauls'];
    /** 404 error bool*/
    bblDetailsNotFound = false;
    /** view child containers */
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('filter') filter: ElementRef;
    clientWidth: number = 0;
    pageSizeOptions: number[] = [5, 10, 25, 100];
    selectedUser: any;
    selectedUserBool: boolean = false;

    constructor(private appService: AppService, private router: Router) {
    }

    ngOnInit() {
        this.clientWidth = window.innerWidth - 50;

        // listen to getting bblDetails
        this.appService.fetchbblDetails().subscribe(
            (data: any) => {
                if (data.length === 0) {
                    this.bblDetailsNotFound = true;
                    return;
                }
                console.log("total data==>" , data);
                // clear up variable
                this.bblDetailsNotFound = false;
                this.bblDetails = new MatTableDataSource(data);
                this.bblDetails.paginator = this.paginator;
                this.bblDetails.sort = this.sort;
            });

        // listen to getting fetchPlayerDetails
        this.appService.fetchPlayerDetails('Jonathan Vandiar').subscribe(
            (data: any) => {
                if (data.length === 0) {
                    this.bblDetailsNotFound = true;
                    return;
                }
                this.bblPlayerDetails = data;
                console.log("get_player_info api ===>" , data);
            });

        // listen to getting fetchTeamDetails
        this.appService.fetchTeamDetails('Adelaide Strikers').subscribe(
            (data: any) => {
                if (data.length === 0) {
                    this.bblDetailsNotFound = true;
                    return;
                }
                this.bblTeamDetails = data;
                console.log("get_team_player_list api ===>" , data);
            });
    }

    ngAfterViewInit() {
        // listen to getting bblDetails
        if (this.bblDetails !== undefined)
            this.appService.fetchbblDetails().subscribe(
                (data: any) => {
                    if (data.length === 0) {
                        this.bblDetailsNotFound = true;
                        return;
                    }
                    // clear up variable
                    this.bblDetailsNotFound = false;
                    this.bblDetails = new MatTableDataSource(data);
                    this.bblDetails.paginator = this.paginator;
                    this.bblDetails.sort = this.sort;
                });
        // this will be used first time sorting and pagination
        if (this.appService.bblDetails.length !== 0) {
            this.bblDetails.paginator = this.paginator;
            this.bblDetails.sort = this.sort;
        }
    }

    /**
     * filter bblDetails
     * @param filterValue
     */
    applyFilter(filterValue: string) {
        this.bblDetails.filter = filterValue.trim().toLowerCase();

        if (this.bblDetails.paginator) {
            this.bblDetails.paginator.firstPage();
        }
    }

    getClickData(data) {
        this.selectedUser = data;
        this.selectedUserBool = true;
    }

    /* called when component is being destroyed
     * clean memory or unsubscribe to current events to avoid memory leaks */
    ngOnDestroy() {
        for (const subscription of this.subscriptions) {
            subscription.unsubscribe();
        }
    }
}
