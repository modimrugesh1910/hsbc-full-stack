const EXPRESS_SERVER: string = 'http://localhost:3000/apis/';

/** URL strings used in API calls */
export const URL_API = {
    BBLLIST: function (): string {
        return (EXPRESS_SERVER + 'get_data');
    },
    BBL_PLAYER_LIST: function (name): string {
        return (EXPRESS_SERVER + 'get_player_info/' + name);
    },
    BBL_TEAM_LIST: function (name): string {
        return (EXPRESS_SERVER + 'get_team_player_list/' + name);
    },
};
