// import built-in modules by angular and other third party libraries or frameworks
import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

// import in-app constants
import {Subject} from "rxjs";
import {map} from "rxjs/operators";
import {URL_API} from "./app.constant";
import {MatSnackBar} from "@angular/material";
// import in-app interface
// import in-app provider
// import in-app functions

@Injectable()
export class AppService {
    bblDetails: Array<any> = [];
    bblPlayerDetails: Array<any> = [];
    bblTeamDetails: Array<any> = [];

    constructor(private router: Router, private httpClient: HttpClient, private snackBar: MatSnackBar) {
    }

    /**
     * fetching loan data
     */
    fetchbblDetails(): any {
        /* set up header parameters
         * include access token stored in localStorage */
        let headers: HttpHeaders = new HttpHeaders();

        return this.httpClient.get(URL_API.BBLLIST()).pipe(map((res: any) => {
                this.bblDetails = [];
                this.bblDetails = res;
                return res;
            },
            (err: HttpErrorResponse) => {
                // clear out array when we got 404 call
                this.bblDetails = [];
            }));
    }

    fetchPlayerDetails(name): any {
        /* set up header parameters
 * include access token stored in localStorage */
        let headers: HttpHeaders = new HttpHeaders();

        return this.httpClient.get(URL_API.BBL_PLAYER_LIST(name)).pipe(map((res: any) => {
                this.bblPlayerDetails = [];
                this.bblPlayerDetails = res;
                return res;
            },
            (err: HttpErrorResponse) => {
                // clear out array when we got 404 call
                this.bblPlayerDetails = [];
            }));
    }

    fetchTeamDetails(name): any {
        /* set up header parameters
 * include access token stored in localStorage */
        let headers: HttpHeaders = new HttpHeaders();

        return this.httpClient.get(URL_API.BBL_TEAM_LIST(name)).pipe(map((res: any) => {
                this.bblTeamDetails = [];
                this.bblTeamDetails = res;
                return res;
            },
            (err: HttpErrorResponse) => {
                // clear out array when we got 404 call
                this.bblTeamDetails = [];
            }));
    }
}
